import math
import heapq


class AStar:
    cost = None
    heuristic = None
    _cache = None
    shouldCache = None

    def __init__(self, heuristic, cost=None, shouldCache=False):
        self.heuristic = heuristic
        self.shouldCache = shouldCache
        self.cost = cost

        # Handles the cache. No reason to change this code.
        if self.shouldCache:
            self._cache = {}

    # Get's from the cache. No reason to change this code.
    def _getFromCache(self, problem):
        if self.shouldCache:
            return self._cache.get(problem)

        return None

    # Get's from the cache. No reason to change this code.
    def _storeInCache(self, problem, value):
        if not self.shouldCache:
            return

        self._cache[problem] = value

    # Run A*
    def run(self, problem):
        # Check if we already have this problem in the cache.
        # No reason to change this code.
        source = problem.initialState
        if self.shouldCache:
            res = self._getFromCache(problem)

            if res is not None:
                return res
        # Initializes the required sets
        closed_set = set()  # The set of nodes already evaluated.
        parents = {}  # The map of navigated nodes.

        # Save the g_score and f_score for the open nodes
        g_score = {source: 0}
        h_score = {source: self.heuristic.estimate(problem, problem.initialState)}
        runCnt = 0
        open_heap = []
        heapq.heappush(open_heap, (h_score[source], runCnt, source))
        developed = 0

        while open_heap:
            f_value, currRunCnt, state = heapq.heappop(open_heap)
            closed_set.add(state)
            if problem.isGoal(state):
                ans = (self._reconstructPath(parents, state), g_score[state], h_score[source], developed)
                if self.shouldCache:
                    self._storeInCache(problem, ans)
                return ans
            developed += 1
            for suc, g in problem.expandWithCosts(state, self.cost):
                runCnt += 1
                new_g = g + g_score[state]
                h_score[suc] = self.heuristic.estimate(problem, suc)
                if suc in [x[2] for x in open_heap]:
                    self.handleOpen(g_score, h_score, new_g, runCnt, open_heap, parents, state, suc)
                elif suc in closed_set:
                    self.handleClosed(closed_set, g_score, h_score, new_g, runCnt, open_heap, parents, state, suc)
                else:
                    parents[suc] = state
                    g_score[suc] = new_g
                    heapq.heappush(open_heap, (g_score[suc] + h_score[suc], runCnt, suc))

        # Returns a tuple of (path, g_score(goal), h(I), developed)
        ans = ([], -1, -1, developed)
        if self.shouldCache:
            self._storeInCache(problem, ans)
        return ans

    def handleClosed(self, closed_set, g_score, h_score, new_g, runCnt, open_heap, parents, state, suc):
        if g_score[suc] > new_g:
            g_score[suc] = new_g
            parents[suc] = state
            closed_set.remove(suc)
            heapq.heappush(open_heap, (g_score[suc] + h_score[suc], runCnt, suc))

    def handleOpen(self, g_score, h_score, new_g, runCnt, open_heap, parents, state, suc):
        if g_score[suc] > new_g:
            g_score[suc] = new_g
            parents[suc] = state
            index = [x[2] for x in open_heap].index(suc)
            heap_remove(open_heap, index)
            heapq.heappush(open_heap, (g_score[suc] + h_score[suc], runCnt, suc))

    # Reconstruct the path from a given goal by its parent and so on
    def _reconstructPath(self, parents:dict, goal):
        path = [goal]
        curr = goal
        while curr in parents.keys():
            curr = parents[curr]
            path.insert(0, curr)
        return path


def heap_remove(heap, index):
    while index > 0:
        up = math.floor((index + 1) / 2) - 1
        heap[index] = heap[up]
        index = up

    heapq.heappop(heap)