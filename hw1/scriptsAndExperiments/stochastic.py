from consts import Consts
from astar import AStar
from ways import load_map_from_csv
from busSolvers import GreedyBestFirstSolver, GreedyStochasticSolver
from problems import BusProblem
from costs import L2DistanceCost
from heuristics import L2DistanceHeuristic
import numpy as np
from scipy import stats

REPEATS = 150

# Load the files
roads = load_map_from_csv(Consts.getDataFilePath("israel.csv"))
prob = BusProblem.load(Consts.getDataFilePath("HAIFA_100.in"))

mapAstar = AStar(L2DistanceHeuristic(), shouldCache=True)

scorer = L2DistanceCost(roads)

# Run the greedy solver
pickingPath = GreedyBestFirstSolver(roads, mapAstar, scorer).solve(prob)
greedyDistance = pickingPath.getDistance() / 1000
print("Greedy solution: {:.2f}km".format(greedyDistance))

# Run the stochastic solver #REPATS times
solver = GreedyStochasticSolver(roads, mapAstar, scorer,
                                Consts.STOCH_INITIAL_TEMPERATURE,
                                Consts.STOCH_TEMPERATURE_DECAY_FUNCTION,
                                Consts.STOCH_TOP_SCORES_TO_CONSIDER)
results = np.zeros((REPEATS,))
minResults = np.zeros((REPEATS,))
currMin = np.iinfo(np.int32).max
print("Stochastic repeats:")
for i in range(REPEATS):
    print("{}..".format(i+1), end=" ", flush=True)
    results[i] = solver.solve(prob).getDistance() / 1000
    currMin = min(currMin, results[i])
    minResults[i] = currMin

print("\nDone!")

from matplotlib import pyplot as plt

plt.title("Greedy vs Greedy Stochastic")
plt.axhline(y=greedyDistance, label="Greedy", color='g')
plt.plot(range(REPEATS), minResults, label="Stochastic", color='r')

plt.legend()
plt.xlabel('Num of iterations')
plt.ylabel('Solution distance')
plt.show()


tStat, pVal = stats.ttest_1samp(results, greedyDistance)
print("P-Value is: {}".format(pVal))
print("Mean is: {}".format(np.mean(results)))
print("STD is: {}".format(np.std(results)))