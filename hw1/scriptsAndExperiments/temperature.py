import numpy as np
from matplotlib import pyplot as plt

INITIAL_T = 0.01
MAX_T = 5

X = np.array([400,900,390,1000,550])

def drawProbs():
    alpha = np.amin(X)

    tDict = {}
    for T in calcT():
        numerators = [np.power(x, -1/T) for x in X]
        denominator = np.sum(numerators)
        tDict[T] = [np.divide(numerator, denominator) for numerator in numerators]

    plt.title("Probabilities as a function of the temperature")
    for i in range(len(X)):
        plt.plot(tDict.keys(), [x[i] for x in tDict.values()], label=str(X[i]))

    plt.legend()
    plt.xlabel('T')
    plt.ylabel('P')
    plt.show()

    # DONE : Write the code as explained in the instructions

def calcT():
    t = INITIAL_T
    while t < MAX_T:
        yield t
        t += (MAX_T + INITIAL_T) / 100

if __name__ == "__main__":
    drawProbs()