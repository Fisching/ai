from . import Heuristic
from ways import tools


# Use the L2 aerial distance (in meters)
class L2DistanceHeuristic(Heuristic):
    def estimate(self, problem, state):
        return tools.compute_distance(problem._roads[problem.target.junctionIdx].coordinates,
                                      problem._roads[state.junctionIdx].coordinates)

