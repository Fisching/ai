from heuristics import Heuristic

class TSPCustomHeuristic(Heuristic):
    _distMat = None
    _junctionToMatIdx = None

    def __init__(self, roads, initialState):
        super().__init__()
        self.roads = roads
    # Estimate heuristically the minimal cost from the given state to the problem's goal
    def estimate(self, problem, state):
        from ways import tools
        if not state.waitingOrders:
            return 0
        all_dists = [tools.compute_distance(self.roads[state.junctionIdx].coordinates,
                                            self.roads[waiting[0]].coordinates)
                     for waiting in state.waitingOrders]
        return max(all_dists) if all_dists else 0
