from . import GreedySolver
import numpy as np

class GreedyStochasticSolver(GreedySolver):
    _TEMPERATURE_DECAY_FACTOR = None
    _INITIAL_TEMPERATURE = None
    _N = None

    def __init__(self, roads, astar, scorer, initialTemperature, temperatureDecayFactor, topNumToConsider):
        super().__init__(roads, astar, scorer)

        self._INITIAL_TEMPERATURE = initialTemperature
        self._TEMPERATURE_DECAY_FACTOR = temperatureDecayFactor
        self._N = topNumToConsider

    def _getSuccessorsProbabilities(self, currState, successors):
        # Get the scores
        X = np.array([self._scorer.compute(currState, target) for target in successors])
        X_sorted = np.sort(X)

        # Initialize an all-zeros vector for the distribution
        P = np.zeros((len(successors)))

        alpha = X_sorted[0]
        r = min(self._N, X.size)
        for i in range(r):
            currIndex = np.where(X == X_sorted[i])
            if len(currIndex[0]) > r - i:
                currIndexTag = (currIndex[0][:r-i],)
            else:
                currIndexTag = currIndex
            P[currIndexTag] = np.divide(X[currIndexTag], alpha)
            P[currIndexTag] = np.power(P[currIndexTag], -1.0 / self.T)

        denominator = np.sum(P)
        P = P / denominator

        # Update the temperature
        self.T *= self._TEMPERATURE_DECAY_FACTOR

        return P

    # Find the next state to develop
    def _getNextState(self, problem, currState):
        successors = list(problem.expand(currState))
        P = self._getSuccessorsProbabilities(currState, successors)

        # Choose the next state stochastically according to the calculated distribution.
        nextIdx = np.random.choice(len(successors), 1, p=P)[0]

        return successors[nextIdx]

    # Override the base solve method to initialize the temperature
    def solve(self, initialState):
        self.T = self._INITIAL_TEMPERATURE
        return super().solve(initialState)