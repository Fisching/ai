import collections
from utils import INFINITY
from operator import itemgetter

char_number = 30
common_threshold = 70
fileDir = r"book.gam"
newFileDir = r"new_book2.gam"


def main():
    moves_count_dict = collections.defaultdict(lambda: 0)
    moves_max_popular_dict = collections.defaultdict(lambda: -INFINITY)
    with open(fileDir, 'r') as f:
        for line in f:
            moves_count_dict[line[0:char_number]] += 1
            moves_max_popular_dict[line[0:char_number]] = max(moves_max_popular_dict[line[0:char_number]], int(line.split(' ')[1]))
    # get 70 most common
    common_dict = dict(sorted(moves_max_popular_dict.items(), key=itemgetter(1), reverse=True)[:common_threshold])
    with open(newFileDir, 'w') as f:
        for key in common_dict.keys():
            f.write("{}: {}\n".format(key, common_dict[key]))


if __name__ == "__main__":
    main()
