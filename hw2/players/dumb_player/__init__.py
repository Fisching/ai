
#===============================================================================
# Imports
#===============================================================================

import abstract
from utils import INFINITY, run_with_limited_time, ExceededTimeError, MiniMaxAlgorithm
from Reversi.consts import EM, OPPONENT_COLOR, BOARD_COLS, BOARD_ROWS
import time
import copy
from collections import defaultdict

#===============================================================================
# Player
#===============================================================================

class Player(abstract.AbstractPlayer):
    def __init__(self, setup_time, player_color, time_per_k_turns, k):
        abstract.AbstractPlayer.__init__(self, setup_time, player_color, time_per_k_turns, k)
        self.clock = time.time()

        # We are simply providing (remaining time / remaining turns) for each turn in round.
        # Taking a spare time of 0.05 seconds.
        self.turns_remaining_in_round = self.k
        self.time_remaining_in_round = self.time_per_k_turns
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05

        # TODO: :'( :'( :'(
        self.point_to_score = {
            # 1s
            (0, 0): 99,
            (BOARD_ROWS - 1, 0): 99,
            (0, BOARD_COLS - 1): 99,
            (BOARD_ROWS - 1, BOARD_COLS - 1): 99,
            # 2s
            (0 + 2, 0): 8,
            (0, 0 + 2): 8,
            (BOARD_ROWS - 1 - 2, 0): 8,
            (BOARD_ROWS - 1, 0 + 2): 8,
            (0 + 2, BOARD_COLS - 1): 8,
            (0, BOARD_COLS - 1 - 2): 8,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1): 8,
            (BOARD_ROWS - 1, BOARD_COLS - 1 - 2): 8,
            # 3s
            (0 + 2, 0 + 2): 7,
            (BOARD_ROWS - 1 - 2, 0 + 2): 7,
            (0 + 2, BOARD_COLS - 1 - 2): 7,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1 - 2): 7,
        }

        self.NORMALIZATION_FACTOR_POWER_SPOTS = 1
        self.NORMALIZATION_FACTOR_NUM_OF_DISCS = 8
        self.NORMALIZATION_FACTOR_STABLE_DISCS = 3
        self.NORMALIZATION_FACTOR_LEGAL_MOVES = 2

    def get_move(self, game_state, possible_moves):
        self.clock = time.time()
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05
        if len(possible_moves) == 1:
            return possible_moves[0]

        min_max = MiniMaxAlgorithm(self.utility, self.color, self.no_more_time, self.selective_deepening_criterion)
        depth = 1
        max_val, max_move = min_max.search(game_state, depth, True)
        try:
            while (not self.no_more_time()):
                depth += 1
                curr_val, curr_move = min_max.search(game_state, depth, True)
                if self.no_more_time():
                    break
                max_val, max_move = curr_val, curr_move
        finally:
            if self.turns_remaining_in_round == 1:
                self.turns_remaining_in_round = self.k
                self.time_remaining_in_round = self.time_per_k_turns
            else:
                self.turns_remaining_in_round -= 1
                self.time_remaining_in_round -= (time.time() - self.clock)

            return max_move

    def utility(self, state):
        if len(state.get_possible_moves()) == 0:
            return INFINITY if state.curr_player != self.color else -INFINITY

        my_u = 0
        op_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_u += 1
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_u += 1

        if my_u == 0:
            # I have no tools left
            return -INFINITY
        elif op_u == 0:
            # The opponent has no tools left
            return INFINITY
        else:
            return my_u - op_u
        
    def selective_deepening_criterion(self, state):
        # Simple player does not selectively deepen into certain nodes.
        return False

    def no_more_time(self):
        return (time.time() - self.clock) >= self.time_for_current_move

    def __repr__(self):
        return '{} {}'.format(abstract.AbstractPlayer.__repr__(self), 'dumb')

# c:\python35\python.exe run_game.py 3 3 3 y simple_player random_player
