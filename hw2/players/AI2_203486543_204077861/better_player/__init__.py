
#===============================================================================
# Imports
#===============================================================================
from itertools import product

import abstract
from players.AI2_203486543_204077861.utils import INFINITY, run_with_limited_time, ExceededTimeError
from Reversi.consts import EM, OPPONENT_COLOR, BOARD_COLS, BOARD_ROWS

import time
from operator import itemgetter
from os import path
import copy
from collections import defaultdict

#===============================================================================
# Player
#===============================================================================
fileDir = path.abspath(path.join(r'Reversi', r'book.gam'))
LAST_COL = BOARD_COLS - 1
NUM_OF_OPEN_MOVES = 10
CHAR_NUMBER = NUM_OF_OPEN_MOVES * 3
COMMON_THRESHOLD = 70


def mirror_cols(tup):
    return LAST_COL - tup[0], tup[1]


def ccw_spin(tup):
    return tup[1], LAST_COL - tup[0]


def mirror_rows(tup):
    return tup[0], LAST_COL - tup[1]


def cw_spin(tup):
    return LAST_COL - tup[1], tup[0]


BOARD2BOOK_DICT = {(5,3): ccw_spin,
                   (4,2): mirror_cols,
                   (2,4): cw_spin,
                   (3,5): mirror_rows}


BOOK2BOARD_DICT = {(5,3): cw_spin,
                   (4,2): mirror_cols,
                   (2,4): ccw_spin,
                   (3,5): mirror_rows}


class Player(abstract.AbstractPlayer):
    def __init__(self, setup_time, player_color, time_per_k_turns, k):
        abstract.AbstractPlayer.__init__(self, setup_time, player_color, time_per_k_turns, k)
        self.clock = time.time()

        # We are simply providing (remaining time / remaining turns) for each turn in round.
        # Taking a spare time of 0.05 seconds.
        self.turns_remaining_in_round = self.k
        self.time_remaining_in_round = self.time_per_k_turns
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05

        # STATIC ANALYSIS OF BOARD
        self.point_to_score = {
            # 1s
            (0, 0): 99,
            (BOARD_ROWS - 1, 0): 99,
            (0, BOARD_COLS - 1): 99,
            (BOARD_ROWS - 1, BOARD_COLS - 1): 99,
            # 2s
            (0 + 2, 0): 8,
            (0, 0 + 2): 8,
            (BOARD_ROWS - 1 - 2, 0): 8,
            (BOARD_ROWS - 1, 0 + 2): 8,
            (0 + 2, BOARD_COLS - 1): 8,
            (0, BOARD_COLS - 1 - 2): 8,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1): 8,
            (BOARD_ROWS - 1, BOARD_COLS - 1 - 2): 8,
            # 3s
            (0 + 2, 0 + 2): 7,
            (BOARD_ROWS - 1 - 2, 0 + 2): 7,
            (0 + 2, BOARD_COLS - 1 - 2): 7,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1 - 2): 7,
        }

        # Some articles we read said we should consider weighting the heuristic in that order
        # We fine-tuned it, but not too much
        self.NORMALIZATION_FACTOR_POWER_SPOTS = 1
        self.NORMALIZATION_FACTOR_NUM_OF_DISCS = 8
        self.NORMALIZATION_FACTOR_STABLE_DISCS = 3
        self.NORMALIZATION_FACTOR_LEGAL_MOVES = 2

        # Opening book
        self.initial_pieces_on_board = [(3, 3), (3, 4), (4, 3), (4, 4)]
        self.useBook = False
        self.currentBoard = None
        self.movesList = []
        self.movesDict = {}

        # create moves dict
        moves_count_dict = defaultdict(lambda: 0)
        moves_best_avg_dict = defaultdict(lambda: 0)
        with open(fileDir, 'r') as f:
            for line in f:
                moves_count_dict[line[0:CHAR_NUMBER]] += 1
                moves_best_avg_dict[line[0:CHAR_NUMBER]] = (moves_best_avg_dict[line[0:CHAR_NUMBER]] + int(line.split(' ')[1]))
            for key, value in moves_best_avg_dict.items():
                moves_best_avg_dict[key] /= moves_count_dict[key]
        # get most common
        # temp_dict = dict(sorted(moves_best_avg_dict.items(), key=itemgetter(1), reverse=(player_color == 'X'))[ :min(COMMON_THRESHOLD, len(moves_best_avg_dict.keys()) - 1)])
        temp_dict = dict(sorted(moves_count_dict.items(), key=itemgetter(1), reverse=True)[:min(COMMON_THRESHOLD, len(moves_best_avg_dict.keys()) - 1)])
        for line in temp_dict.keys():
            minus_split_list = line[1:].split(':')[0].split('-')
            min_plus_split_list = [s.split('+') for s in minus_split_list]
            flat_list = [(ord(item[0]) - ord('a'), int(item[1]) - 1) for sublist in min_plus_split_list for item in sublist]
        for i in range(len(flat_list)):
            if repr(flat_list[:i]) in self.movesDict.keys():
                continue
        self.movesDict[repr(flat_list[:i])] = flat_list[i]

    def get_move(self, game_state, possible_moves):
        self.clock = time.time()
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05
        if len(possible_moves) == 1:
            # print("no other option, do: {}".format(possible_moves[0]))
            return possible_moves[0]
        best_move = None
        # addition for opening moves
        if self.useBook and len(self.movesList) <= NUM_OF_OPEN_MOVES:
            best_move = self.openning_move(game_state)
            # print("best move from playbook is {}".format(best_move))

        # print("current board is {}None".format("" if self.currentBoard is None else "not "))
        next_util = 0
        if not best_move:
            best_move = possible_moves[0]
            next_state = copy.deepcopy(game_state)
            next_state.perform_move(best_move[0], best_move[1])
            next_util = self.utility(next_state)
            # Choosing an arbitrary move
            # Get the best move according the utility function
            for move in possible_moves:
                new_state = copy.deepcopy(game_state)
                new_state.perform_move(move[0], move[1])
                new_util = self.utility(new_state)
                if self.utility(new_state) > next_util:
                    next_util = new_util
                    next_state = new_state
                    best_move = move

        if self.turns_remaining_in_round == 1:
            self.turns_remaining_in_round = self.k
            self.time_remaining_in_round = self.time_per_k_turns
        else:
            self.turns_remaining_in_round -= 1
            self.time_remaining_in_round -= (time.time() - self.clock)
        # print("best move chosen is {} with util {}".format(best_move, next_util))
        return best_move

    def utility(self, state):
        my_u = 0
        op_u = 0
        my_discs, op_discs, board_heu = self.count_discs_and_board(state)

        if len(state.get_possible_moves()) == 0:
            if my_discs > op_discs:
                return INFINITY
            elif my_discs < op_discs:
                return -INFINITY
            return board_heu

        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_u += self.is_stable(state, x, y, self.color) / self.NORMALIZATION_FACTOR_STABLE_DISCS
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_u += self.is_stable(state, x, y,
                                           OPPONENT_COLOR[self.color]) / self.NORMALIZATION_FACTOR_STABLE_DISCS

        return my_u - op_u + \
               (my_discs - op_discs) / self.NORMALIZATION_FACTOR_NUM_OF_DISCS + \
               board_heu + \
               (-1) * self.heu_num_of_legal_moves(state) / self.NORMALIZATION_FACTOR_LEGAL_MOVES

    def count_discs_and_board(self, state):
        my_discs = 0
        op_discs = 0
        my_u = 0
        op_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_discs += 1
                    if self.is_power_spot(x, y):
                        my_u += self.point_to_score[(x, y)] / self.NORMALIZATION_FACTOR_POWER_SPOTS
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_discs += 1
                    if self.is_power_spot(x, y):
                        op_u += self.point_to_score[(x, y)] / self.NORMALIZATION_FACTOR_POWER_SPOTS

        return my_discs, op_discs, (my_u - op_u)

    def heu_stable_discs(self, state):
        my_u = 0
        op_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_u += self.is_stable(state, x, y, self.color)
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_u += self.is_stable(state, x, y, OPPONENT_COLOR[self.color])

        return my_u - op_u

    def is_stable(self, state, x, y, color):
        my_corners = [
            (0, 0, 1, 1),
            (0, BOARD_COLS - 1, 1, -1),
            (BOARD_ROWS - 1, 0, -1, 1),
            (BOARD_ROWS - 1, BOARD_COLS - 1, -1, -1),
        ]

        for point_x, point_y, step_x, step_y in my_corners:
            stable = True
            for i in range(point_x, x + step_x, step_x):
                for j in range(point_y, y + step_y, step_y):
                    if state.board[i][j] != color:
                        stable = False
                if not stable:
                    break
            if stable:
                return True

        return False

    def heu_power_spots(self, state):
        my_u = 0
        op_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    if self.is_power_spot(x, y):
                        my_u += self.point_to_score[(x, y)]
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    if self.is_power_spot(x, y):
                        op_u += self.point_to_score[(x, y)]

        return my_u - op_u

    def is_power_spot(self, x, y):
        return (x, y) in self.point_to_score

    def heu_num_of_legal_moves(self, state):
        state.curr_player = OPPONENT_COLOR[state.curr_player]
        num_of_legal_moves = len(state.get_possible_moves())
        state.curr_player = OPPONENT_COLOR[state.curr_player]
        return num_of_legal_moves

    def findLastMove(self, state):
        possible_moves = product(range(BOARD_COLS), range(BOARD_ROWS))
        filtered_list = [(x, y) for (x, y) in possible_moves if state.board[x][y] == OPPONENT_COLOR[self.color] and
                self.currentBoard[x][y] == EM and self.board2book((x, y)) != self.movesList[-1]]
        # print("filtered_list: {}".format(filtered_list))
        assert len(filtered_list) == 1
        return filtered_list[0]

        # for x in range(BOARD_COLS):
        #     for y in range(BOARD_ROWS):
        #         if self.currentBoard[x][y] == EM:
        #             if state.board[x][y] == OPPONENT_COLOR[self.color]:
        #                 return x, y
        # return None

    def pieces_on_board(self, game_state):
        possible_moves = product(range(BOARD_COLS), range(BOARD_ROWS))
        return [(x, y) for (x,y) in possible_moves if game_state.board[x][y] != EM]


    def openning_move(self, game_state):
        current_key = []

        if self.currentBoard is not None:  # not our first move!
            self.movesList.append(self.board2book(self.findLastMove(game_state)))
            current_key = self.movesList
        else:
            self.board2book = BOARD2BOOK_DICT[(5, 3)]
            self.book2board = BOOK2BOARD_DICT[(5, 3)]
            pieces_on_board = self.pieces_on_board(game_state)
            # print("pieces_on_board: {}".format(pieces_on_board))
            if len(pieces_on_board) > 4:
                current_move = [tup for tup in pieces_on_board if tup not in self.initial_pieces_on_board][0]
                # print("current_move: {}".format(current_move))
                self.board2book = BOARD2BOOK_DICT[current_move]
                self.book2board = BOOK2BOARD_DICT[current_move]
                current_key = [self.board2book(current_move)]
                # print("current_key: {}".format(current_key))
        if repr(current_key) not in self.movesDict.keys():
            self.useBook = False
            return None
        my_move = self.movesDict[repr(current_key)]
        self.currentBoard = copy.deepcopy(game_state.board)
        self.movesList.append(my_move)
        # print("my_move = {}, book2board(my_move) = {}".format(my_move, self.book2board(my_move)))
        return self.book2board(my_move)

    # end of opening moves

    def selective_deepening_criterion(self, state):
        # Simple player does not selectively deepen into certain nodes.
        return False

    def no_more_time(self):
        return (time.time() - self.clock) >= self.time_for_current_move

    def __repr__(self):
        return '{} {}'.format(abstract.AbstractPlayer.__repr__(self), 'better')

# c:\python35\python.exe run_game.py 3 3 3 y simple_player random_player
