
#===============================================================================
# Imports
#===============================================================================

import abstract
from players.AI2_203486543_204077861.utils import INFINITY, run_with_limited_time, ExceededTimeError, MiniMaxWithAlphaBetaPruning, MiniMaxWithAlphaBetaPruningAndCaching
from Reversi.consts import EM, OPPONENT_COLOR, BOARD_COLS, BOARD_ROWS
import time
import copy
from collections import defaultdict
from operator import itemgetter
from itertools import product
import functools

#===============================================================================
# Player
#===============================================================================
LAST_COL = BOARD_COLS - 1


class Player(abstract.AbstractPlayer):
    def __init__(self, setup_time, player_color, time_per_k_turns, k):

        abstract.AbstractPlayer.__init__(self, setup_time, player_color, time_per_k_turns, k)
        self.clock = time.time()

        # We are simply providing (remaining time / remaining turns) for each turn in round.
        # Taking a spare time of 0.05 seconds.
        self.turns_remaining_in_round = self.k
        self.time_remaining_in_round = self.time_per_k_turns
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05

        self.point_to_score = {
            # 1s
            (0, 0): 99,
            (BOARD_ROWS - 1, 0): 99,
            (0, BOARD_COLS - 1): 99,
            (BOARD_ROWS - 1, BOARD_COLS - 1): 99,
            # 2s
            (0 + 2, 0): 8,
            (0, 0 + 2): 8,
            (BOARD_ROWS - 1 - 2, 0): 8,
            (BOARD_ROWS - 1, 0 + 2): 8,
            (0 + 2, BOARD_COLS - 1): 8,
            (0, BOARD_COLS - 1 - 2): 8,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1): 8,
            (BOARD_ROWS - 1, BOARD_COLS - 1 - 2): 8,
            # 3s
            (0 + 2, 0 + 2): 7,
            (BOARD_ROWS - 1 - 2, 0 + 2): 7,
            (0 + 2, BOARD_COLS - 1 - 2): 7,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1 - 2): 7,
            # bad spots
            (0 + 1, 0 + 1): -24,
            (BOARD_ROWS - 1 - 1, 0 + 1): -24,
            (0 + 1, BOARD_COLS - 1 - 1): -24,
            (BOARD_ROWS - 1 - 1, BOARD_COLS - 1 - 1): -24,

            (0 + 1, 0): -8,
            (BOARD_ROWS - 1 - 1, 0): -8,
            (0 + 1, BOARD_COLS - 1): -8,
            (BOARD_ROWS - 1 - 1, BOARD_COLS - 1): -8,

            (0, 0 + 1): -8,
            (0, BOARD_ROWS - 1 - 1): -8,
            (BOARD_COLS - 1, 0 + 1): -8,
            (BOARD_COLS - 1, BOARD_ROWS - 1 - 1): -8,
        }
        # see comments in better player regarding these factors
        self.NORMALIZATION_FACTOR_POWER_SPOTS = 1
        self.NORMALIZATION_FACTOR_STABLE_DISCS = 3
        self.NORMALIZATION_FACTOR_NUM_OF_DISCS = 8
        self.NORMALIZATION_FACTOR_LEGAL_MOVES = 2
        self.power_spot_temp = 0.85


    def get_move(self, game_state, possible_moves):
        self.clock = time.time()
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05
        if len(possible_moves) == 1:
            return possible_moves[0]

        alpha_beta = MiniMaxWithAlphaBetaPruning(self.utility,
                                                           self.color,
                                                           self.no_more_time,
                                                           self.selective_deepening_criterion)
        depth = 1
        max_val, max_move = alpha_beta.search(game_state, depth, -INFINITY, INFINITY, True)
        try:
            while not self.no_more_time():
                depth += 1
                curr_val, curr_move = alpha_beta.search(game_state, depth, -INFINITY, INFINITY, True)
                if self.no_more_time():
                    break
                max_val, max_move = curr_val, curr_move
        finally:
            if self.turns_remaining_in_round == 1:
                self.turns_remaining_in_round = self.k
                self.time_remaining_in_round = self.time_per_k_turns
            else:
                self.turns_remaining_in_round -= 1
                self.time_remaining_in_round -= (time.time() - self.clock)

            self.NORMALIZATION_FACTOR_POWER_SPOTS *= self.power_spot_temp
            return max_move

    def utility(self, state):
        my_u = 0
        op_u = 0
        my_discs, op_discs, board_heu = self.count_discs_and_board(state)
        # print("DEBUG: my_discs, op_discs, board_heu =  {} {} {}".format(my_discs, op_discs, board_heu))

        if len(state.get_possible_moves()) == 0:
            if my_discs > op_discs:
                return INFINITY
            elif my_discs < op_discs:
                return -INFINITY
            return board_heu

        my_u += self.heu_stable_discs(state, self.color) / self.NORMALIZATION_FACTOR_STABLE_DISCS
        op_u += self.heu_stable_discs(state, OPPONENT_COLOR[self.color]) / self.NORMALIZATION_FACTOR_STABLE_DISCS

        return my_u - op_u + \
               (my_discs - op_discs) / self.NORMALIZATION_FACTOR_NUM_OF_DISCS + \
               board_heu + \
               (-1) * self.heu_num_of_legal_moves(state) / self.NORMALIZATION_FACTOR_LEGAL_MOVES

    def count_discs_and_board(self, state):
        pairs = product(range(BOARD_COLS), range(BOARD_ROWS))
        res = [self.count_disc_and_board_per_square(state, x, y) for (x, y) in pairs]
        return functools.reduce(lambda x, y: (x[0] + y[0], x[1] + y[1], x[2] + y[2]), res)

    # return (is_my_disc, is_rivals_disc, net_gain_to_utility)
    def count_disc_and_board_per_square(self, state, x, y):
        res = [0, 0, 0]
        if state.board[x][y] == EM:
            return res

        res[0 if state.board[x][y] == self.color else 1] += 1  # add 1 to whomever owns the disk
        if self.is_power_spot(x, y):                           # if this is a power spot, add its value if it is ours, subtract if it isn't
            res[2] += (1 if state.board[x][y] == self.color else -1) * self.point_to_score[(x, y)] / self.NORMALIZATION_FACTOR_POWER_SPOTS
        return tuple(res)

    def heu_stable_discs(self, state, color):
        directions = [
            [(0, 0), (0, BOARD_COLS - 1), (0, 1)],
            [(0, 0), (BOARD_COLS - 1, 0), (1, 0)],

            [(0, BOARD_COLS -1), (BOARD_COLS - 1, BOARD_COLS - 1), (1, 0)],
            [(0, BOARD_COLS -1), (0, 0), (0, -1)],

            [(BOARD_COLS - 1, 0), (0, 0), (-1, 0)],
            [(BOARD_COLS - 1, 0), (BOARD_COLS - 1, BOARD_COLS - 1), (0, 1)],

            [(BOARD_COLS - 1, BOARD_COLS - 1), (BOARD_COLS - 1, 0), (0, -1)],
            [(BOARD_COLS - 1, BOARD_COLS - 1), (0, BOARD_COLS - 1), (-1, 0)],
        ]
        stable = 0

        for dir in directions:
            broken_line = False
            x,y = dir[0]
            while x <= dir[1][0] and y <= dir[1][1]:
                if state.board[x][y] != color:
                    broken_line = True
                    break
                stable += 1
                x += dir[2][0]
                y += dir[2][1]

            if not broken_line:
                stable -= 4

        return stable

    def is_power_spot(self, x, y):
        return (x, y) in self.point_to_score

    def heu_num_of_legal_moves(self, state):
        state.curr_player = OPPONENT_COLOR[state.curr_player]
        num_of_legal_moves = len(state.get_possible_moves())
        state.curr_player = OPPONENT_COLOR[state.curr_player]
        return num_of_legal_moves

    def selective_deepening_criterion(self, state):
        # Simple player does not selectively deepen into certain nodes.
        return False

    def no_more_time(self):
        return (time.time() - self.clock) >= self.time_for_current_move

    def __repr__(self):
        return '{} {}'.format(abstract.AbstractPlayer.__repr__(self), 'competition')

# c:\python35\python.exe run_game.py 3 3 3 y simple_player random_player
