
#===============================================================================
# Imports
#===============================================================================

import abstract
from players.AI2_203486543_204077861.utils import INFINITY, run_with_limited_time, ExceededTimeError, MiniMaxWithAlphaBetaPruning
from Reversi.consts import EM, OPPONENT_COLOR, BOARD_COLS, BOARD_ROWS
import time
import copy
from collections import defaultdict

#===============================================================================
# Player
#===============================================================================

class Player(abstract.AbstractPlayer):
    def __init__(self, setup_time, player_color, time_per_k_turns, k):
        abstract.AbstractPlayer.__init__(self, setup_time, player_color, time_per_k_turns, k)
        self.clock = time.time()

        # We are simply providing (remaining time / remaining turns) for each turn in round.
        # Taking a spare time of 0.05 seconds.
        self.turns_remaining_in_round = self.k
        self.time_remaining_in_round = self.time_per_k_turns
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05

        self.point_to_score = {
            # 1s
            (0, 0): 99,
            (BOARD_ROWS - 1, 0): 99,
            (0, BOARD_COLS - 1): 99,
            (BOARD_ROWS - 1, BOARD_COLS - 1): 99,
            # 2s
            (0 + 2, 0): 8,
            (0, 0 + 2): 8,
            (BOARD_ROWS - 1 - 2, 0): 8,
            (BOARD_ROWS - 1, 0 + 2): 8,
            (0 + 2, BOARD_COLS - 1): 8,
            (0, BOARD_COLS - 1 - 2): 8,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1): 8,
            (BOARD_ROWS - 1, BOARD_COLS - 1 - 2): 8,
            # 3s
            (0 + 2, 0 + 2): 7,
            (BOARD_ROWS - 1 - 2, 0 + 2): 7,
            (0 + 2, BOARD_COLS - 1 - 2): 7,
            (BOARD_ROWS - 1 - 2, BOARD_COLS - 1 - 2): 7,
        }
        # see comment in better player
        self.NORMALIZATION_FACTOR_POWER_SPOTS = 1
        self.NORMALIZATION_FACTOR_NUM_OF_DISCS = 8
        self.NORMALIZATION_FACTOR_STABLE_DISCS = 3
        self.NORMALIZATION_FACTOR_LEGAL_MOVES = 2

    def get_move(self, game_state, possible_moves):
        self.clock = time.time()
        self.time_for_current_move = self.time_remaining_in_round / self.turns_remaining_in_round - 0.05
        if len(possible_moves) == 1:
            return possible_moves[0]

        alpha_beta = MiniMaxWithAlphaBetaPruning(self.utility,
                                                 self.color,
                                                 self.no_more_time,
                                                 self.selective_deepening_criterion)
        depth = 1
        max_val, max_move = alpha_beta.search(game_state, depth, -INFINITY, INFINITY, True)
        try:
            while (not self.no_more_time()):
                depth += 1
                curr_val, curr_move = alpha_beta.search(game_state, depth, -INFINITY, INFINITY, True)
                if self.no_more_time():
                    break
                max_val, max_move = curr_val, curr_move
        finally:
            if self.turns_remaining_in_round == 1:
                self.turns_remaining_in_round = self.k
                self.time_remaining_in_round = self.time_per_k_turns
            else:
                self.turns_remaining_in_round -= 1
                self.time_remaining_in_round -= (time.time() - self.clock)
            print("depth on exit: {}".format(depth))
            return max_move

    def utility(self, state):
        my_u = 0
        op_u = 0
        my_discs, op_discs, board_heu = self.count_discs_and_board(state)

        if len(state.get_possible_moves()) == 0:
            if my_discs > op_discs:
                return INFINITY
            elif my_discs < op_discs:
                return -INFINITY
            return board_heu

        # TODO: beautify code
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_u += self.is_stable(state, x, y, self.color) / self.NORMALIZATION_FACTOR_STABLE_DISCS
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_u += self.is_stable(state, x, y, OPPONENT_COLOR[self.color]) / self.NORMALIZATION_FACTOR_STABLE_DISCS

        return my_u - op_u + \
               (my_discs - op_discs) / self.NORMALIZATION_FACTOR_NUM_OF_DISCS + \
               board_heu + \
               (-1) * self.heu_num_of_legal_moves(state) / self.NORMALIZATION_FACTOR_LEGAL_MOVES

    def count_discs_and_board(self, state):
        my_discs = 0
        op_discs = 0
        my_u = 0
        op_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_discs += 1
                    if self.is_power_spot(x, y):
                        my_u += self.point_to_score[(x,y)] / self.NORMALIZATION_FACTOR_POWER_SPOTS
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_discs += 1
                    if self.is_power_spot(x, y):
                        op_u += self.point_to_score[(x,y)] / self.NORMALIZATION_FACTOR_POWER_SPOTS

        return my_discs, op_discs, (my_u - op_u)

    def heu_stable_discs(self, state):
        my_u = 0
        op_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    my_u += self.is_stable(state, x, y, self.color)
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    op_u += self.is_stable(state, x, y, OPPONENT_COLOR[self.color])

        return my_u - op_u

    def is_stable(self, state, x, y, color):
        my_corners = [
            (0, 0, 1, 1),
            (0, BOARD_COLS - 1, 1, -1),
            (BOARD_ROWS - 1, 0, -1, 1),
            (BOARD_ROWS - 1, BOARD_COLS - 1, -1, -1),
        ]

        for point_x, point_y, step_x, step_y in my_corners:
            stable = True
            for i in range(point_x, x + step_x, step_x):
                for j in range(point_y, y + step_y, step_y):
                    if state.board[i][j] != color:
                        stable = False
                if not stable:
                    break
            if stable:
                return True

        return False

    def heu_power_spots(self, state):
        my_u = 0
        op_u = 0
        for x in range(BOARD_COLS):
            for y in range(BOARD_ROWS):
                if state.board[x][y] == self.color:
                    if self.is_power_spot(x, y):
                        my_u += self.point_to_score[(x,y)]
                if state.board[x][y] == OPPONENT_COLOR[self.color]:
                    if self.is_power_spot(x, y):
                        op_u += self.point_to_score[(x,y)]

        return my_u - op_u

    def is_power_spot(self, x, y):
        return (x,y) in self.point_to_score

    def heu_num_of_legal_moves(self, state):
        state.curr_player = OPPONENT_COLOR[state.curr_player]
        num_of_legal_moves = len(state.get_possible_moves())
        state.curr_player = OPPONENT_COLOR[state.curr_player]
        return num_of_legal_moves

        
    def selective_deepening_criterion(self, state):
        # Simple player does not selectively deepen into certain nodes.
        return False

    def no_more_time(self):
        return (time.time() - self.clock) >= self.time_for_current_move

    def __repr__(self):
        return '{} {}'.format(abstract.AbstractPlayer.__repr__(self), 'alpha_beta')

# c:\python35\python.exe run_game.py 3 3 3 y simple_player random_player
