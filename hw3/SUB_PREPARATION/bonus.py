import sklearn.tree as tree
from itertools import combinations
import copy
import sklearn.metrics as metrics
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split, cross_val_score
import sfs
import csv

CLASS_COL = 4

def my_score(clf, x, y):
    test_accuracies = cross_val_score(clf, x, y)
    return sum(test_accuracies) / float(len(test_accuracies))

def col_to_enum(train, curr):
    my_dict = {}
    for i in range(len(train)):
        if train[i][curr] not in my_dict:
            my_dict[train[i][curr]] = len(my_dict)
        train[i][curr] = my_dict[train[i][curr]]

def check_all_features(x_train, y_train):
    '''return best subset of features
       assuming x_train is passed the e-num translation
    '''

    best_score = 0
    best_feature_set = []
    num_of_features = len(x_train[0])
    for i in range(1, num_of_features + 1):
        print(i)
        for features in combinations(range(num_of_features), i):
            # current train subset
            curr_train = create_train_subset(features, x_train)
            # clf = tree.DecisionTreeClassifier(criterion="entropy", random_state=3, min_samples_leaf=11)
            clf = tree.DecisionTreeClassifier(criterion="entropy", random_state=3)
            curr_res = cross_val_score(clf, curr_train, y_train, cv=10)
            avg_score = sum(curr_res) / float(len(curr_res))
            if avg_score > best_score:
                best_score = avg_score
                best_feature_set = features
    print(best_score)
    return best_feature_set


def create_train_subset(features, x_train):
    curr_train = [[] for k in x_train]
    for feat_i in features:
        [curr_train[k].append(x_train[k][feat_i]) for k in range(len(x_train))]
    return curr_train


def classify(clf, file, features):
    with open(file, 'r') as f:
        reader = csv.reader(f)
        dataset_with_labels = list(reader)
    with open(file, 'r') as f:
        reader = csv.reader(f)
        dataset_backup = list(reader)[1:]
    dataset_with_labels[0].append('clarity')
    dataset = dataset_with_labels[1:]
    col_to_enum(dataset, 2)
    col_to_enum(dataset, 3)
    dataset = [dataset[i][1:] for i in range(len(dataset))]
    dataset = create_train_subset(features, dataset)
    results = clf.predict(dataset)
    [dataset_backup[i].append(results[i]) for i in range(len(dataset))]
    with open(r'unlabeled_diamonds.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(dataset_with_labels[0])
        writer.writerows(dataset_backup)


# load data
with open(r'diamonds.csv', 'r') as f:
        reader = csv.reader(f)
        dataset = list(reader)
dataset = dataset[1:]
col_to_enum(dataset, 2)
col_to_enum(dataset, 3)

x_train, x_test, y_train, y_test = train_test_split([t[1:CLASS_COL] + t[CLASS_COL+1:] for t in dataset], [t[CLASS_COL] for t in dataset], test_size=0.1)

feature_set = check_all_features(x_train, y_train)
curr_x_train = create_train_subset(feature_set, x_train)
curr_x_test = create_train_subset(feature_set, x_test)
clf_over = tree.DecisionTreeClassifier(criterion="entropy", random_state=3)
clf_over.fit(curr_x_train, y_train)
print("clf_over = {}".format(clf_over.score(curr_x_test, y_test)))
classify(clf_over, r'unlabeled_diamonds.csv', feature_set)


