import sklearn.tree as tree
import sklearn.metrics as metrics
import numpy as np
import csv

NUM_OF_FOLDS = 4

# load data
with open(r'flare.csv', 'r') as f:
        reader = csv.reader(f)
        dataset = list(reader)
dataset = dataset[1:]

num_of_items = len(dataset)
# split to folds
dataset_folds = []
for i in range(NUM_OF_FOLDS):
    dataset_folds.append(dataset[i * num_of_items // NUM_OF_FOLDS :(i + 1) * num_of_items // NUM_OF_FOLDS])

# train and test
all_mats = []
mat_sum = np.ndarray([2, 2])
sum_accuracy = 0
for i in range(NUM_OF_FOLDS):
    classifier = tree.DecisionTreeClassifier(criterion="entropy", random_state=3)
    training = []
    for j in range(NUM_OF_FOLDS):
        if i == j: continue
        training.extend(dataset_folds[j])
    classifier.fit([t[:-1] for t in training], [t[-1] for t in training])
    sum_accuracy += classifier.score([t[:-1] for t in dataset_folds[i]], [t[-1] for t in dataset_folds[i]])
    predictions = classifier.predict([t[:-1] for t in dataset_folds[i]])
    classes = [t[-1] for t in dataset_folds[i]]
    all_mats.append(metrics.confusion_matrix(classes, predictions))

# print average
print("{}".format(sum_accuracy / NUM_OF_FOLDS))
print("{}".format(np.sum(all_mats, axis=0)))


