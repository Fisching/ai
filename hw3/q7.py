import sklearn.tree as tree
import sklearn.metrics as metrics
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split, cross_val_score
import sfs
import csv

def my_score(clf, x, y):
    test_accuracies = cross_val_score(clf, x, y)
    return sum(test_accuracies) / float(len(test_accuracies))

# load data
with open(r'flare.csv', 'r') as f:
        reader = csv.reader(f)
        dataset = list(reader)
dataset = dataset[1:]

x_train, x_test, y_train, y_test = train_test_split([t[:-1] for t in dataset], [t[-1] for t in dataset], test_size=0.25)

kclf = KNeighborsClassifier()
kclf.fit(x_train, y_train)
knn_score = kclf.score(x_test, y_test)

print(knn_score)


clf = KNeighborsClassifier()
k_features = sfs.sfs(x_train, y_train, 8, clf, my_score)
sfs_x_train = [[] for i in range(len(x_train))]
sfs_x_test = [[] for i in range(len(x_test))]
for i in range(len(k_features)):
    [sfs_x_train[j].append(x_train[j][k_features[i]]) for j in range(len(x_train))]
    [sfs_x_test[j].append(x_test[j][k_features[i]]) for j in range(len(x_test))]

clf.fit(sfs_x_train, y_train)
sfs_score = clf.score(sfs_x_test, y_test)

print(sfs_score)