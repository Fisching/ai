import sklearn.tree as tree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split, cross_val_score
import sfs
import csv

def my_score(clf, x, y):
    test_accuracies = cross_val_score(clf, x, y)
    return sum(test_accuracies) / float(len(test_accuracies))

# load data
with open(r'flare.csv', 'r') as f:
        reader = csv.reader(f)
        dataset = list(reader)
dataset = dataset[1:]

x_train, x_test, y_train, y_test = train_test_split([t[:-1] for t in dataset], [t[-1] for t in dataset], test_size=0.25)

# Q7
# Section a
kclf = KNeighborsClassifier() # default K is 5, as requested
kclf.fit(x_train, y_train)
knn_score = kclf.score(x_test, y_test)

print(knn_score)

# Section b
clf = KNeighborsClassifier() # default K is 5, as requested
b_features = sfs.sfs(x_train, y_train, 8, clf, my_score)
sfs_x_train = [[] for i in range(len(x_train))]
sfs_x_test = [[] for i in range(len(x_test))]
for i in range(len(b_features)):
    [sfs_x_train[j].append(x_train[j][b_features[i]]) for j in range(len(x_train))]
    [sfs_x_test[j].append(x_test[j][b_features[i]]) for j in range(len(x_test))]

clf.fit(sfs_x_train, y_train)
sfs_score = clf.score(sfs_x_test, y_test)

print(sfs_score)

# Q8
# Section a
clf_no_prunning = tree.DecisionTreeClassifier(criterion="entropy", random_state=3)
clf_no_prunning.fit(x_train, y_train)
no_prunning = clf_no_prunning.score(x_test, y_test)
print(no_prunning)

# Section b
clf_with_prunning = tree.DecisionTreeClassifier(criterion="entropy", random_state=3, min_samples_leaf=20)
clf_with_prunning.fit(x_train, y_train)
with_prunning = clf_with_prunning.score(x_test, y_test)
print(with_prunning)

# tree_clf = tree.DecisionTreeClassifier(criterion="entropy", random_state=3)
# k_features = sfs.sfs(x_train, y_train, 8, tree_clf, my_score)
# sfs_x_train = [[] for i in range(len(x_train))]
# sfs_x_test = [[] for i in range(len(x_test))]
# for i in range(len(k_features)):
#     [sfs_x_train[j].append(x_train[j][k_features[i]]) for j in range(len(x_train))]
#     [sfs_x_test[j].append(x_test[j][k_features[i]]) for j in range(len(x_test))]
#
# tree_clf.fit(sfs_x_train, y_train)
# sfs_score = tree_clf.score(sfs_x_test, y_test)
#
# print(sfs_score)