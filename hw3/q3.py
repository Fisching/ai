import sklearn.tree as tree
import sklearn.metrics as metrics
import csv

NUM_OF_FOLDS = 4

def print_tree_accuracy(classifier, dataset_folds):
    training = []
    i = 0
    for j in range(len(dataset_folds)):
        if i == j: continue
        training.extend(dataset_folds[j])

    global test_accuracy, train_accuracy
    classifier.fit([t[:-1] for t in training], [t[-1] for t in training])
    test_accuracy = classifier.score([t[:-1] for t in dataset_folds[i]], [t[-1] for t in dataset_folds[i]])
    train_accuracy = classifier.score([t[:-1] for t in training], [t[-1] for t in training])

    print("{}".format(train_accuracy))

# load data
with open(r'flare.csv', 'r') as f:
        reader = csv.reader(f)
        dataset = list(reader)
dataset = dataset[1:]

num_of_items = len(dataset)
# split to folds
dataset_folds = []
for i in range(NUM_OF_FOLDS):
    dataset_folds.append(dataset[i * num_of_items // NUM_OF_FOLDS :(i + 1) * num_of_items // NUM_OF_FOLDS])

# train and test
over_fit_classifier = tree.DecisionTreeClassifier(criterion="entropy", random_state=3)
under_fit_classifier = tree.DecisionTreeClassifier(criterion="entropy", random_state=3, max_depth=1)


print_tree_accuracy(over_fit_classifier, dataset_folds)
print_tree_accuracy(under_fit_classifier, dataset_folds)