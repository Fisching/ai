import sklearn.tree as tree
import sklearn.metrics as metrics
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split, cross_val_score
import sfs
import csv

# load data
with open(r'flare.csv', 'r') as f:
        reader = csv.reader(f)
        dataset = list(reader)
dataset = dataset[1:]

x_train, x_test, y_train, y_test = train_test_split([t[:-1] for t in dataset], [t[-1] for t in dataset], test_size=0.25)

clf_no_prunning = tree.DecisionTreeClassifier(criterion="entropy", random_state=3)
clf_no_prunning.fit(x_train, y_train)
no_prunning = clf_no_prunning.score(x_test, y_test)
print(no_prunning)

clf_with_prunning = tree.DecisionTreeClassifier(criterion="entropy", random_state=3, min_samples_leaf=20)
clf_with_prunning.fit(x_train, y_train)
with_prunning = clf_with_prunning.score(x_test, y_test)
print(with_prunning)