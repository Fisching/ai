import copy

def sfs(x, y, k, clf, score):
    """
    :param x: feature set to be trained using clf. list of lists.
    :param y: labels corresponding to x. list.
    :param k: number of features to select. int
    :param clf: classifier to be trained on the feature subset.
    :param score: utility function for the algorithm, that receives clf, feature subset and labeles, returns a score. 
    :return: list of chosen feature indexes
    """
    features = []
    chosen_features = [[] for i in range(len(x))]
    for curr_feature in range(k):
        curr_best_score = -1
        curr_best_feature = -1
        for i in range(len(x[0])):
            if (i in features):
                continue
            # setup check for new feature
            curr_chosen_features = copy.deepcopy(chosen_features)
            [curr_chosen_features[j].append(x[j][i]) for j in range(len(x))]
            # train & test
            test_score = score(clf, curr_chosen_features, y)
            # update best
            if curr_best_score < test_score:
                curr_best_score = test_score
                curr_best_feature = i
        # after checking all, commit to chosen features
        [chosen_features[j].append(x[j][curr_best_feature]) for j in range(len(x))]
        features.append(curr_best_feature)

    return features